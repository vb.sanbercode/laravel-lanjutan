<?php

// use Illuminate\Http\Request;

// /*
// |--------------------------------------------------------------------------
// | API Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register API routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | is assigned the "api" middleware group. Enjoy building your API!
// |
// */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::namespace('Auth')->group(function () {
  Route::post('register','RegisterApiController');
  Route::post('login', 'LoginApiController');
  Route::post('logout', 'LogoutController');
});

Route::namespace('Article')->middleware('auth:api')->group(function (){
  Route::post('create-new-article','ArticleController@store');
  Route::patch('update-the-selected-article/{article}','ArticleController@update');
  Route::delete('delete-the-selected-article/{article}','ArticleController@destroy');
});

Route::get('articles/{article}','Article\ArticleController@show');

Route::get('articles','Article\ArticleController@index');

Route::get('user','UserApiController');


Route::group([
  'middleware' => 'api',
  'prefix' => 'auth',
  'namespace' => 'Auth'
  ], function(){
    Route::post('register','RegisterSanbercodeController');
    Route::post('regenerate-otp','RegenerateOtpCodeController'); 
    Route::post('verification','VerificationSanbercodeController');
    Route::post('update-password','UpdatePasswordController');
    Route::post('login','LoginSanbercodeController');
  }
);

Route::group([
  'middleware' => ['api','email_verified','auth:api'],
],function(){
  Route::get('profile/show','ProfileController@show');
  Route::post('profile/update','ProfileController@update');
});

Route::get('todo','TodoController@index');
Route::post('todo','TodoController@store');
Route::post('todo/change-done-status/{id}','TodoController@changeDoneStatus');
Route::post('todo/delete/{id}','TodoController@delete');

Route::group([
  'middleware' => 'api',
  'prefix'     => 'campaign',
], function(){
  Route::get('random/{count}','CampaignController@random');
  Route::post('store','CampaignController@store');
  Route::get('/','CampaignController@index');
  Route::get('/{id}','CampaignController@detail');
});

Route::group([
  'middleware' => 'api',
  'prefix'     => 'blog',
], function(){
  Route::get('random/{count}','BlogController@random');
  Route::post('store','BlogController@store');
});