<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUUid;

class OtpCode extends Model
{
    //
    use UsesUuid;

    protected $guarded = [];
}
