<?php

namespace App\Models\Article;

use Illuminate\Database\Eloquent\Model;

use App\Models\UserApi;

class Article extends Model
{
    //
    protected $fillable = ['title','slug','body','subject_id'];

    protected $with = ['subject', 'user'];

    public function getRouteKeyName()
    {
      return 'slug';
    }


    public function user()
    {
      return $this->belongsTo(UserApi::class,'user_id');
    }
    
    public function subject()
    {
      return $this->belongsTo(Subject::class,'id');
    }

}
