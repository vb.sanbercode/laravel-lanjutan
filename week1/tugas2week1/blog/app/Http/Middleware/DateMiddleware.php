<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class DateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      
        $currect_date = Carbon::now()->day;
  
        if($currect_date >=1 && $currect_date <=10){
          return $next($request);
        }
  
        abort(403);
        
    }
}
