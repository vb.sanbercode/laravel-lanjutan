<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;

use App\Events\UserRegisterdeEvent;
use App\Events\UserRegistered;

class RegisterSanbercodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        $request->validate([
          'email' => 'required|unique:users,email|email',
          'name' => 'required',
        ]);

        $data = [];
          
        $data_request = $request->all();
        $user = User::create($data_request);
          
        $data['user'] = $user;

        event(new UserRegisterdeEvent($user));
        event(new UserRegistered($user,'register'));
          
        return response()->json([
          'reponse_code' => '00',
          'reponse_message' => 'user baru berhasil didaftarkan, silahkan cek email untuk melihat kode otp',
          'data' => $data
        ],200);

    }
}
