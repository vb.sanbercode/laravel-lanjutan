<?php

namespace App\Http\Controllers\Auth;

use App\UserApi;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\Auth\RegisterRequest;

class RegisterApiController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        //
        UserApi::create([
          'name' => request('name'),
          'email' => request('email'),
          'password' => Hash::make(request('password'))
        ]);

        return response('Thaks,you are registered.');
        
    }
}
