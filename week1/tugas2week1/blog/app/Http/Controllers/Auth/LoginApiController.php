<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginApiController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        // $request->validate([
        //   'name' => 'required',
        //   'email' => 'required',
        //   'password' => 'required'
        //   ]);

        if(!$token = auth()->attempt($request->only('email','password'))){
           return response(null,401);
        };

        return response()->json(compact('token'));

    }
}
