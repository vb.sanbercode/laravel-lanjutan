<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Campaign;

class CampaignController extends Controller
{
    public function random($count)
    {
      $campaigns = Campaign::select('*')
                  ->inRandomOrder()
                  ->limit($count)
                  ->get();

      $data['campaigns'] = $campaigns;

      return response()->json([
        'response_code' => '00',
        'response_message' => 'data campaigns berhasil ditampilkan',
        'data' => $data
      ],200);
    }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $campaigns = Campaign::paginate(1);

        $data['campaigns'] = $campaigns;

        return response()->json([
          'response_code' => '00',
          'response_message' => 'data campaigns berhasil ditampilkan',
          'data' => $data
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request -> validate([
          'title' => 'required',
          'description' => 'required',
          'image' => 'required|mimes:jpg,jpeg,png'
        ]);

        $campaign = Campaign::create([
          'title' => $request->title,
          'description' => $request->description,
        ]);

        if($request->hasFile('image')){

          $image = $request->file('image');
          $image_extension = $image->getClientOriginalExtension();
          $image_name = $campaign->id. "." .$image_extension;
          $image_folder = '/photos/campaign/';
          $image_location = $image_folder . $image_name;

          try{
            $image->move(public_path($image_folder),$image_name);

            $campaign->update([
              'image' => $image_location,
            ]);
          }catch(\Exception $e){
            return response()->json([
              'response_code' => '01',
              'response_message' => 'photo profile gagal upload',
              'data' => $data,
            ],200);
          }

        }

        $data['campaign'] = $campaign;

        return response()->json([
          'response_code' => '00',
          'response_message' => 'data campaign berhasil ditambahkan',
          'data' => $data,
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        //
        $campaign = Campaign::find($id);

        $data['campaign'] = $campaign;

        return response()->json([
          'response_code' => '00',
          'rensponse_message' => 'data campaign berhasil ditampilkan',
          'data' => $data,
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
