<?php

namespace App\Listeners;

use App\Events\UserRegisterdeEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use App\Mail\UserRegisterdeMail;
use Mail;

class SendEmailNotificationUserRegistered implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegisterdeEvent  $event
     * @return void
     */
    public function handle(UserRegisterdeEvent $event)
    {
        //
        Mail::to($event->user)->send(new UserRegisterdeMail($event->user));

    }
}
