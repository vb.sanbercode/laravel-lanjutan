<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUUid;

class Role extends Model
{
    //
    use UsesUuid;

    protected $guarded = [];
}
