<?php

trait Hewan 
{

  public static $nama;
  public static $darah = 50;
  public static $jumlahKaki;
  public static $keahlian;

  public static function atraksi() {
     //return echo("{$this->$nama}");
     echo static::$nama." sedang ".static::$keahlian."<br>";
  }

  // public static function setNama($nama){
  //    static::$nama = $nama;
  // }

}

trait Fight
{

  public static $attackPower;
  public static $defencePower;

  public static function serang($var1,$var2) {
    echo $var1." sedang menyerang ".$var2."<br>";
  }

  public static function diserang($var1,$var2,$darah) {
    echo $var1." sedang diserang ".$var2."<br>";

    return $darah - static::$attackPower/static::$defencePower;

  }

}

class Elang
{

  use Hewan;
  use Fight;

  public $lawan;

  public function __construct($nama,$lawan) {
    Hewan::$nama=$nama;
    Hewan::$jumlahKaki = 2;
    Hewan::$keahlian="terbang tinggi";

    Fight::$attackPower=10;
    Fight::$defencePower=5;

    $this->lawan=$lawan;
	}

  public function getInfoHewan(){
    Hewan::atraksi();

    Fight::serang(Hewan::$nama,$this->lawan);

    Hewan::$darah = Fight::diserang(Hewan::$nama,$this->lawan, Hewan::$darah);
  }
}

class Harimau
{
  use Hewan;
  use Fight;

  public $lawan;

  public function __construct($nama,$lawan) {
    Hewan::$nama=$nama;
    Hewan::$jumlahKaki = 4;
    Hewan::$keahlian="lari cepat";

    Fight::$attackPower=7;
    Fight::$defencePower=8;

    $this->lawan=$lawan;
	}

  public function getInfoHewan(){
    Hewan::atraksi();
    Fight::serang(Hewan::$nama,$this->lawan);
    Hewan::$darah = Fight::diserang(Hewan::$nama,$this->lawan, Hewan::$darah);
  }

}

$elang = new Elang("Elang_3","harimau");
$elang->getInfoHewan();

$harimau = new Harimau("Harimau_1","elang");
$harimau->getInfoHewan();

?>