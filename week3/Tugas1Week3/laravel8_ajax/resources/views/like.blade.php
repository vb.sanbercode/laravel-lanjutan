<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>

  <div id="app">
    <form>
      <input type="text" v-model="form.name">
      <button v-show="!updateSubmit" @@click.prevent="add">Add</button>
      <button @@click.prevent="update" v-show="updateSubmit">Update</button>
    </form>

    <ul v-for="(user, index) in users">
      <li>
        <span>@{{ user.name }}</span>
        <button @@click="edit(user, index)">Edit</button> ||
        <button @@click="del(user, index)">Delete</button>
      </li>

    </ul>
  </div>
  
  <script src="https://cdn.jsdelivr.net/npm/vue"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

  <script>

    var app = new Vue({
      el : '#app',
      data(){
        return {
          users : [
            // {
            //   'name' : 'Muhammad Iqbal Mubarok'
            // },
            // {
            //   'name' : 'Ruby Purwanti'
            // },
            // {
            //   'name' : 'Faqih Muhammad Iqbal'
            // }
          ],
          updateSubmit : false,
          form : {
            'name' : '',
            'id' : ''
          },
          selectedUserId : null,
        }
      },
      methods:{
        add(){
          // POST /someUrl
          this.$http.post('/api/like', 
          {
            name : this.form.name, 
            email : this.form.name + '@gmail.com',
          }).then(response => {

            this.users.push(
              this.form
            )

            this.form = {}

          });
          
        },
        edit(user, index){
          this.selectedUserId = index
          this.updateSubmit = true
          this.form.name = user.name
          this.form.id = user.id
        },
        update(){

          this.$http.post('/api/like/change-name',{
            id : this.form.id,
            name : this.form.name,
          }).then(response => {
            
          });
          
          this.users[this.selectedUserId].name = this.form.name

          this.form = {}
          this.updateSubmit = false
          this.selectedUserId = null
        },
        del(user, index){
          var r = confirm('Anda Yakin ??')

          if(r){
            this.$http.post('/api/like/delete/' + user.id).then(response => {
              this.users.splice(index,1)});
            
          }

        }
      },
      mounted() {
        // GET /someUrl
        this.$http.get('/api/like').then(response => {
          let result = response.body.data;
          this.users = result;
        });
      }
    })

  </script>
  
</body>
</html>