<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Http\Resources\LikeResource;

class LikeController extends Controller
{
    //
    public function index(){
      $users = User::all();

      return LikeResource::collection($users);
    }

    public function store(Request $request){
      $user = User::create([
        'name' => $request->name,
        'email' => $request->email,
      ]);

      return new LikeResource($user);

    }

    public function delete($id){
      User::destroy($id);
      return 'success';
    }


    public function changeName(Request $request){
      
      $user = User::find($request->id);

      $user->update([
        'name' => $request->name,
      ]);

      return new LikeResource($user);
    }

}
